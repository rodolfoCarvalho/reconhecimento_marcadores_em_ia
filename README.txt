Trabalho realizado pelos alunos: Daniel de Faria Godoi e Rodolfo Borges

Como executar o programa:
	python trabalho_2_ia.py

Comandos da câmera:
	Digite (q) para sair.
	Digite (r) para gravar.
	Digite (c) para mudar de classe.

Programas a serem abertos:
	gedit -------- Desenhar (G) na imagem
	libreoffice -- Desenhar (W) na imagem
	firefox ------ Desenhar (I) na imagem
	sublime ------ Desenhar (S) na imagem
	calculadora -- Desenhar (C) na imagem
