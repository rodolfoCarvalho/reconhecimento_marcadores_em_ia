"""
TRABALHO 2 - INTELIGENCIA ARTIFICIAL
@author Daniel de Faria Godoi
@author Rodolfo Borges
"""
# Importacoes realizadas
import cv2
import numpy as np
import csv
import math
import subprocess


#Funcao responsavel por capturar imagens da webcam
def capture_webcam(limit,color_weak,color_hard,color_bgr,qtde_frames):
	print "Digite (r) para gravar o movimento ou (c) para mudar de classe\n"
	if limit==0:
		classe = input("Insira o numero da classe inicial: ")
	pos_x = 0
	pos_y = 0
	set_lines = []
	set_points = []
	cap = cv2.VideoCapture(0)
	l = []
	cap.set(3,700)
	cap.set(4,600)
	cont = 0
	example = []
	continued = 1
	record = 0
	radius = 20
	
	while continued:
	   	ret, frame_ponto = cap.read()
	   	frame_ponto = cv2.flip(frame_ponto, 1)
		hsv = cv2.cvtColor(frame_ponto, cv2.COLOR_BGR2HSV)
		mask = cv2.inRange(hsv, color_weak, color_hard)
		mom = cv2.moments(mask)
		ordem_2 = mom['m00']
		if ordem_2 != 0:
			ordem_1_x = mom['m10']
			ordem_1_y = mom['m01']
			ultimapos_x = pos_x
			ultimapos_y = pos_y
			pos_x = int(ordem_1_x / ordem_2)
			pos_y = int(ordem_1_y / ordem_2)
			ponto_b = (pos_x + radius, pos_y + radius)
			ponto_a = (pos_x - radius, pos_y - radius)
			cv2.rectangle(frame_ponto, ponto_a, ponto_b, color_bgr, 3)
			if record==1:
				set_points.append([pos_x,pos_y])
				if (ultimapos_y > 0) and (ultimapos_x > 0) and (pos_y > 0) and (pos_x > 0):
					set_lines.append([(pos_x, pos_y),(ultimapos_x,ultimapos_y)])
					for i in set_lines:
						cv2.line(frame_ponto, i[0], i[1], color_bgr, 3)
				if len(set_points) > qtde_frames:
					if limit==0:
						example.append([classe,set_points])
					else:
						example = set_points
						continued=0
					cont = cont+1
					if(limit==0):
						print 'Exemplos lidos: ' + str(cont)
					set_lines = []
					set_points = []
					record = 0
					pos_x = 0
					pos_y = 0
		else:
			set_lines = []
			set_points = []
			record = 0
			pos_x = 0
			pos_y = 0
		cv2.imshow('imagem rabiscada', frame_ponto)
		key = cv2.waitKey(1)
		if key & 0xFF == ord('r'):
			record=1
		if key & 0xFF == ord('q') and record==0:
			continued=0
		if key & 0xFF == ord('c') and record==0:
			classe = input("Digite o numero da nova classe: ")
			cont=0
			set_lines = []
			set_points = []
			record = 0
			pos_x = 0
			pos_y = 0
	cap.release()
	cv2.destroyAllWindows()
	return example

#Funcao responsavel por gravar os treinamentos realizados
def training(color_weak,color_hard,color_bgr,qtde_frames):
	csvfile = open('treinamento.csv', 'a')
	writer = csv.writer(csvfile, lineterminator = '\n')
	example = capture_webcam(0,color_weak,color_hard,color_bgr,qtde_frames)
	for row in example:
		writer.writerow(row)
	csvfile.close()

#Funcao responsavel por normalizar um vetor (0-1)
def normalize(array):
	min_y = 1000
	max_y = 0
	min_x = 1000
	max_x = 0
	for x in array:
		if x[0]>max_x:
			max_x = x[0]
		if x[0]<min_x:
			min_x = x[0]
		if x[1]>max_y:
			max_y = x[1]
		if x[1]<min_y:
			min_y = x[1]
	min_x = float(min_x)
	max_x = float(max_x)
	min_y = float(min_y)
	max_y = float(max_y)
	for x in array:
		x[0] = (float(x[0]) - min_x)/(max_x-min_x)
		x[1] = (float(x[1]) - min_y)/(max_y-min_y)
	return array

#Funcao responsavel por dar suporte a funcao sorted() para ordenamento
def order(a):
	return a[0]

#Funcao responsavel por calcular a distancia euclidiana entre dois pontos
def euclides_distance(a,b):
	return math.sqrt(((a[0]-b[0])**2)+((a[1]-b[1])**2))

#Funcao responsavel por calcular similaridade entre duas series temporais
def dtw_dist(array_a,array_b):
	a = len(array_a)
	b = len(array_b)
	mat = []
	for i in range(a):
		mat.append([])
		for j in range(b):
			mat[i].append([])
	aux1 = 0
	aux2 = 0
	while aux1<a:
		aux2=0
		while aux2<b:
			mat[aux1][aux2] = euclides_distance(array_a[aux1],array_b[aux2]) * euclides_distance(array_a[aux1],array_b[aux2])
			aux2 = aux2 + 1
		aux1 = aux1 + 1
	dist=0
	a = a-1
	b = b-1
	while (a>0 or b>0):
		dist = dist + mat[a][b]
		aux1 = a-1
		aux2 = b-1
		if aux1>=0:
			if aux2>=0:
				if mat[aux1][b] < mat[a][aux2]:
					if mat[aux1][b] < mat[aux1][aux2]:
						a = aux1
					else:
						a = aux1
						b = aux2
				elif mat[a][aux2]<mat[aux1][aux2]:
					b = aux2
				else:
					a = aux1
					b = aux2
			else:
				a = aux1
		else:
			b = aux2
	dist = dist + mat[0][0]
	return dist

#Funcao responsavel por classificar exemplos de acordo com o algoritmo KNN
def knnDecision(n_neighbors,array_x,array_y,array_scan):
	distance = []
	aux=0
	for ex in array_x:
		distance.append([dtw_dist(ex,array_scan),aux])
		aux = aux + 1
	distance = sorted(distance,key=order)
	return array_y[distance[0][1]]

	return 0;

#Funcao responsavel por realizar os testes usando os trenamentos para predicao
def tests(color_weak,color_hard,color_bgr,qtde_frames):
	csvfile = open('treinamento.csv', 'r')
	reader = csv.reader(csvfile)
	cont = 0
	array_x = []
	array_y = []
	for row in reader:
		move = []
		cont2=0
		for x in row:
			x = x.replace(']','')
			x = x.replace('[','')
			x = x.replace(' ','')
			if cont2==0:
				clas = int(x)
			else:
				x = x.split(',')
				move.append([int(x[0]),int(x[1])])
			cont2 = cont2 + 1
		array_x.append(move)
		array_y.append(clas)
		cont = cont + 1
	csvfile.close()
	aux = 0
	for x in array_x:
		array_x[aux] = (normalize(x))
		aux = aux + 1
	array_scan = capture_webcam(1,color_weak,color_hard,color_bgr,qtde_frames)
	if len(array_scan)>0:
		array_scan = normalize(array_scan)
		clas = knnDecision(1,array_x,array_y,array_scan)
		if clas==1:
			print "Gedit"
			subprocess.call(['gedit'])
		elif clas==2:
			print "LibreOffice"
			subprocess.call(['libreoffice'])
		elif clas==3:
			print "Firefox"
			subprocess.call(['firefox'])
		elif clas==4:
			print "Sublime"
			subprocess.call(['sublime'])
		elif clas==5:
			print "Calculadora"
			subprocess.call(['calc'])


#Inicio da funcao principal
def main():
	tipo = input("Ola, voce gostaria de inserir novos treinamentos (1) ou iniciar os testes (2) ? ")
	red_weak = np.array([179,100,100], dtype=np.uint8)
	red_hard = np.array([179,255,255], dtype=np.uint8)
	yellow_weak = np.array([20,125,100], dtype=np.uint8)
	yellow_hard = np.array([50,255,255], dtype=np.uint8)
	green_weak = np.array([50,80,50], dtype=np.uint8)
	green_hard = np.array([80,255,255], dtype=np.uint8)
	blue_weak = np.array([100,120,50], dtype=np.uint8)
	blue_hard = np.array([135,255,255], dtype=np.uint8)
	purple_weak = np.array([140,90,50], dtype=np.uint8)
	purple_hard = np.array([160,255,255], dtype=np.uint8)
	white_weak = np.array([0,0,200], dtype=np.uint8)
	white_hard = np.array([179,10,255], dtype=np.uint8)
	color=7
	while color < 0 or color > 6:
		color = input("\nEscolha uma cor para ser identificada:\n1 - verde\t2 - azul\t3 - vermelho\n4 - roxo\t5 - branco\t6 - amarelo\n0 - Sair\nDigite um numero: ")

	if color ==0:
		exit()
	color_weak = yellow_weak
	color_hard = yellow_hard
	color_bgr = (0,255,255)
	if color == 3:
		color_weak = red_weak
		color_hard = red_hard
		color_bgr = (0,0,255)
		print "Cor: vermelho"
	if color == 1:
		color_weak = green_weak
		color_hard = green_hard
		color_bgr = (0,255,0)
		print "Cor: verde"
	if color == 2:
		color_weak = blue_weak
		color_hard = blue_hard
		color_bgr = (255,0,0)
		print "Cor: azul"
	if color == 6:
		color_weak = yellow_weak
		color_hard = yellow_hard
		color_bgr = (0,255,255)
		print "Cor: amarelo"
	if color == 4:
		color_weak = purple_weak
		color_hard = purple_hard
		color_bgr = (128,0,128)
		print "Cor: roxo"
	if color == 5:
		color_weak = white_weak
		color_hard = white_hard
		color_bgr = (255,255,255)
		print "Cor: branco"
	qtde_frames = input("Insira o numero de frames a serem escaneados: ")
	print "\nPara sair aperte \"q\", a qualquer momento\n"
	if tipo==1:
		training(color_weak,color_hard,color_bgr,qtde_frames)
	elif tipo==2:
		tests(color_weak,color_hard,color_bgr,qtde_frames)

#chamar funcao principal
main()